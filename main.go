package main

/*
 * @program: reporter
 * @author: vkebrt
 * @version: 0.01
 * @description: generate report .docx from data readed via nessus api
 */

import (
	//	"flag"
	//	"fmt"
	"log"
	"os"
	//	"reporter/pkg/nessusapi"
	//	"reporter/pkg/docreport"
	//	"reporter/pkg/reportmail"
)

func main() {
	// init log subsystem
	iLOG := log.New(os.Stdout, "[Info] ", log.Ldate|log.Ltime|log.Lmicroseconds)
	//	eLOG := log.New(os.Stderr,"[Info] ", log.Ldate|log.Ltime|log.Lmicroseconds)
	//	dLOG := log.New(os.Stdout,"[Info] ", log.Ldate|log.Ltime|log.Lmicroseconds)

	/* start/end informations */
	iLOG.Printf(" reporter starting up...")
	defer iLOG.Printf(" reporter ending...")

}
